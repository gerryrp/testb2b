<html>
<head>

	<meta charset="utf-8">
	<title>B2B Haryono</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php 
    require('../layout/linkcss.php');
    ?>  
	
</head>

<body>

<?php 
require('../layout/headerb2b.php');
?>
      
<br><br>
        

        <div class="container max-w-full">
            <div class="flex flex-wrap overflow-hidden mb-4">
                
                    <div class="w-1/4 overflow-hidden sm: md:w-1/6 lg:w-1/6 xl:w-1/4">

                    </div>

                    <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/4 xl:w-1/4">
                            <div class="px-3 py-3 border-r border-black">
                                <span class="span_title">
                                    Business to Business<br>
                                    Domestic Hotel Reservation<br>
                                </span>
                                <br>	
                                <span class="span_yellow" >
                                 Made for Travel Agent, Corporate, and Hotel users
                                </span>		
                                <br>
                                <span class="span_blue" >
                                    We are now accepting <br>
                                    <a href="https://www.klikbca.com/KlikPay/klikpay.html" class="span_blue3" >BCA KlikPay,</a>
                                    <a href="" class="span_blue3" > CIMB Click</a> and 
                                    <a href="" class="span_blue3" > Mandiri ClickPay</a>
                                    <br><br>		
                                </span>
                            
                                <span style="font-size:11px">
                                    Built and run on Open Source Software<br>
                                    <span class="span_black">Compatible with</span>
                                     <a href="https://www.google.com/intl/en/chrome/" class="span_blue2" >Google Chrome</a>, 
                                     <a href="http://www.mozilla.org/en-US/firefox/new/" class="span_blue2">Mozilla Firefox</a>, & 
                                     <a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads" class="span_blue2">Internet Explorer 9</a>, 
                                     and above<br>
                                    <span class="span_brown">Not compatible with Internet Explorer 8 and below</span><br>
                                    Need help? Contact us at inf...@haryono.co.id<br><br>
                                    Copyright &copy;2014 
                                    <a href="https://www.haryonotours.com/" class="span_blue1">www.haryonotours.com</a>
                                    [BETA]
                                </span>
                            </div>
                    </div>

                    <div class="w-full overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 p-5">

                        <div class="flex flex-wrap overflow-hidden mb-4 ">
                        
                            <div class="w-1/3  overflow-hidden sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/4">
                                <a href="forgot_pass.php" class="span_blue1" style="text-decoration:none;vertical-align:middle">Forgot password ?</a>
                            </div>
                        
                            <div class="w-1/2  overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 ">
                                <button onclick="window.location.href = 'sign_up.php';" type="button" class="btn-danger"  value="Sign Up"  >Sign Up</button>
                            </div>
                        
                        </div>
                    
                        <div class="flex mb-4">
                            
                            <div class="w-1/2">
                                    <input type="text" name="username" class="form_login border border-black" placeholder="Username atau email ..">
                            </div>

                        </div>

                        <div class="flex mb-4">
                            
                            <div class="w-1/2">
                                    <input type="text" name="username" class="form_login border border-black" placeholder="Password ..">
                            </div>

                        </div>

                        <div class="flex mb-4">
                            
                            <div class="w-1/2">
                                    <input type="text" name="username" class="form_login border border-black" placeholder="Company ID ..">
                            </div>

                        </div>

                        <div class="flex flex-wrap overflow-hidden mb-4 ">
                            
                            <div class="w-1/3 overflow-hidden sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/4">
							
									<input type="checkbox" class="check" id="dropdownCheck2"  data-label="remember me">
									<label class="span_blue1" for="dropdownCheck2">
                                            Remember me
									</label>							
							
                            </div>
                                
                            <div class="w-1/2 overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2">
                                <button onclick="window.location.href = 'home.php';" type="button" class="btn-primary" value="Log in"  >Log in</button>
                            </div>
 
                        </div>
                         
                    </div>

            </div>
        </div>

        
        

</body>
</html>