<div  class="container max-w-full">
                <div  class=" mb-4 h-32 sm:h-32 md:h-16 lg:h-16 xl:h-16">

                <nav class="flex md-4 "> 

                   

                    <div class="w-full sm:w-full md:w-full ml-5">
                        <ul>                     
                           <li><a href = 'home.php';>Home</a></li>
                            <li><a href=#>Archives</a>
                                <!-- First Tier Drop Down -->
                                <ul>
                                    <li><a href="#">Archives 1</a></li>
                                    <li><a href="#">Archives 2</a></li>
                                    <li><a href="#">Archives 3</a></li>
                                </ul>        
                            </li>
                            <li><a href="#">Report</a>
                                <!-- First Tier Drop Down -->
                            <ul>
                               <li><a href="#">Report 1</a></li>
                               <li><a href="#">Report 2</a></li>
                                <li><a href="#">Report 3</a>
                                <!-- Second Tier Drop Down -->
                                    <ul>
                                       <li><a href="#">HTML/CSS</a></li>
                                       <li><a href="#">jQuery</a></li>
                                       <li><a href="#">Other</a>
                                           <!-- Third Tier Drop Down -->
                                           <ul>
                                               <li><a href="#">Stuff</a></li>
                                               <li><a href="#">Things</a></li>
                                               <li><a href="#">Other Stuff</a></li>
                                           </ul>
                                       </li>
                                    </ul>
                                </li>
                            </ul>
                            </li>
                            <li><a href="#">Master</a>
                             <!-- First Tier Drop Down -->
                             <ul>
                                 <li><a href="#">Master 1</a></li>
                                 <li><a href="#">Master 2</a></li>
                                 <li><a href="#">Master 3</a></li>
                             </ul>        
                             </li>
                            <li><a href="#">Setting</a>
                             <ul>
                               <li><a href="#">Setting 1</a></li>
                               <li><a href="#">Setting 2</a></li>
                               <li><a href="#">Setting 3</a></li>
                            </ul>
                            </li>
                            <li><a href="#">Profile</a>
                              <ul>
                                <li><a href="#">Profile 1</a></li>
                                <li><a href="#">Profile 2</a></li>
                                <li><a href="#">Profile 3</a></li>
                            </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="flex w-1/3">
                        
                     </div>

                    <ul class="logout">
                        <li>
                            <a href="login.php">
                                <span class="nav-text">
                                    Logout
                                </span>
                            </a>
                        </li>  
                    </ul>

                </nav>
                           
                    
                </div>
            </div>
