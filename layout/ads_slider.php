           <div class="container mt-5 max-w-full sm:mt-3 md:mt-3 lg:mt-3 xl:mt-3">
                    <div class="flex flex-wrap overflow-hidden md-4 bg-orange-lighter p-5">
                        
                            <div class="w-1/3 overflow-hidden sm:w-1/3 md:w-1/3 lg:w-1/3 xl:w-1/3">
                                    <button class="material-icons float-right"> 
                                        keyboard_arrow_left
                                    </button>
                                </div>
                      
                                <div class="w-1/3 overflow-hidden sm:w-1/3 md:w-1/3 lg:w-1/3 xl:w-1/3" style="text-align:center; font-family:'Cabin', sans-serif;" >
                                    THE SULTAN HOTEL & RESIDENCE JAKARTA [*5]
                                    Domestic Hotel Reservation          
                                </div>   
                                
                                <div class="w-1/5 overflow-hidden sm:w-1/3 md:w-1/3 lg:w-1/3 xl:w-1/3">
                                    <button class="material-icons">
                                        keyboard_arrow_right
                                    </button>
                                </div>
    
                    </div>
                </div>

   