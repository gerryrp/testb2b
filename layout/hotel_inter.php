<html>
<head>
<title>B2B Haryono</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">              
        
        <?php 
        require('../layout/linkcss.php');
        ?>

</head>


<body>
<div class="container bg-blue-lightest  max-w-full">

    <div class="container m-auto">
                
            <div class="justify-center items-center">
                <p><small>Registered Customer</small></p>
            </div>
            
            <div class="flex md-4 justify-center items-center">
                <input type="text" class="textareasmall border border-black" placeholder="registered customer" >
                <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
            </div>
                                     
    </div>

    <div class="container max-w-full">
    
        <div class="flex flex-wrap overflow-hidden md-4 bg-blue-lightest p-5">

            <div class="w-1/3">

            </div>
            

            <div class="w-full overflow-hidden sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2">
                    <ul class="list-reset flex ">
                        <li class="-mb-px mr-1">
                          <a onclick="window.location.href = 'menu_hotel_dom.php';" class="bg-white inline-block border-l border-t border-r rounded-t  py-2 px-4 text-blue-dark font-semibold" href="#">Hotel Domestik</a>
                        </li>
                        <li class="mr-1">
                          <a onclick="window.location.href = 'menu_hotel_inter.php';" class="bg-white inline-block py-2 px-4 text-blue-light hover:text-blue-darker font-semibold" href="#">Hotel Internasional</a>
                        </li>
                        <li class="mr-1">
                          <a class="bg-white inline-block py-2 px-4 text-blue-light hover:text-blue-darker font-semibold" href="#">Airlines Domestik</a>
                        </li>
                    </ul>
            </div>

        </div>
            
    </div>
    
    
    
    <div class="container max-w-full ">
    
                <div class="flex flex-wrap overflow-hidden md-4 z-10 ">

                    <div class="w-1/3 overflow-hidden sm:w-1/4 md:w-1/4 lg:w-1/5 xl:w-1/4  ">                    
                                             
                        <label class="dropdown">

                        <div class="dd-button">
                        Dropdown
                        </div>

                        <input type="checkbox" class="dd-input" id="test">

                        <ul class="dd-menu">
                        <li>Action</li>
                        <li>Another action</li>
                        <li>Something else here</li>
                        <li class="divider"></li>
                        <li>
                            <a href="http://rane.io">Link to Rane.io</a>
                        </li>
                        </ul>

                        </label>

                    </div>


                    <div class="w-full pure-form overflow-hidden sm:w-full md:w-full lg:w-1/4 xl:w-1/6">
                        

                        <div class="justify-center items-center">
                            <p><small>Hotel or city name</small></p>
                        </div>

                        <div class="flex md-4 justify-center items-center">
                            <input type="text" class="corners2 border border-black" placeholder="hotel or city name" >
                            <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                        </div>

                        
                    </div> 

                    <div class="w-1/2 overflow-hidden sm:w-full md:w-full lg:w-1/6 xl:w-1/6 ">

                        <div class="flex md-4 justify-center items-center">
                            <p><small>Check-in date:</small></p>
                        </div>

                        <div>
                            <label for="dateofbirth"></label>
                        </div>

                        <div class="flex md-4 justify-center items-center">
                            <input class="flex" type="date" name="dateofbirth" id="dateofbirth" >
                        </div>

                    </div>

                    <div class="w-1/2 overflow-hidden sm:w-full md:w-full lg:w-1/6 xl:w-1/6">   

                            <div class="flex md-4 justify-center items-center">
                                <p><small>Check-out date:</small></p>
                            </div>

                            <div>
                                <label for="dateofbirth"></label>
                            </div>

                            <div class="flex md-4 justify-center items-center">
                                <input class="flex" type="date" name="dateofbirth" id="dateofbirth" >
                            </div>

                    </div>
                   
                </div>
                
    </div>
    
    <div class="container max-w-full ">
    
                <div class="flex md-4 justify-center items-center z-0" >

                        <div class="w-1/5  p-4">

                            <div class="flex md-4 justify-center ">
                            <a href="" class="span_blue1" >
                                Advance Search</a>
                            </div>

                            <div>
                                    Star Rating
                                    1
                                    2
                                    3
                                    4
                                    5

                            </div>

                            <div>
                                    Price
                                    
                            </div>

                        </div>

                        <div class="w-1/5 flex md-4 justify-center  items-center  ">
                                <button type="button" class="btn-secondary mr-5 fa fa-search" value="Sign Up"  > Search</button>
                            
                                <button type="button" class="btn-secondary fa fa-refresh" value="Sign Up"  > Reset</button>
                        </div>  

                </div>
    
    </div>


</div>        




</body>
</html>